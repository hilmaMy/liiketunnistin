import paho.mqtt.client as mqtt
import time
import datetime
import json
import mysql.connector
import serial


# Tietokannan tiedot
with open('config.txt') as json_file:
	tiedot = json.load(json_file)

# Lukee tiedot arduinosta
arduino = serial.Serial('COM3', 9600, timeout=5)

# Funktio jolla tieto lähetetään tietokantaan
def pushToDB():
	
	# Lukee lähetettävän datan
	with open('data.txt') as json_file:
		data = json.load(json_file)
	
	# Luodaan yhteys tietokantaan oman tietokannan tiedoilla.
	conn = mysql.connector.connect(
			host = tiedot['host'],
			user = tiedot['user'],
			passwd = tiedot['pw'],
			database = tiedot['db'])
            
    # Kursori jonka avulla voidaan iteroida taulun rivit
	mycursor = conn.cursor()

	# Lähetettävät muuttujat
	aika = str(datetime.datetime.now())
	valoisuus = data['Valoisuus']
	if valoisuus == 1:
		valoisuus = 0
	else:
		valoisuus = 1
	
	liike = data['Liike']
	led = data['Led']
	
	# tietokantaan lähetettävät arvot
	msg = [valoisuus, liike, led, aika]
	
    # Query joka halutaan ajaa
	sql = ("INSERT INTO Liiketunnistin(valoisuus, liike, led, aikaleima) VALUES (%s, %s, %s, %s)")
    
    # Ajetaan query
	mycursor.execute(sql, msg)
	conn.commit()
	print('New row has been pushed to database')
 
def writeData():
	# Luetaan raakadata
	rawdata = arduino.readline()[:-2] 
	
	# Siistitään data
	result = rawdata.decode("utf-8")
	
	Dicti = dict((x.strip(), y.strip()) for x, y in (element.split(':') for element in result.split(' ')))	
	result = json.dumps(Dicti)
	
	
	# Kirjoitetaan data tiedostoon
	if result:
		with open("data.txt","w") as file:
			file.write(result)
			
def main():
	while True:
		writeData()
		pushToDB()
		time.sleep(5)

main()
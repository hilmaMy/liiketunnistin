const int sensorSignal = 3;
const int ledPin = 13;
const int valoPin = 2;
int sensorValue = 0;
int valoStatus = 0;
int ledStatus= 0;
unsigned long timer = 0;
unsigned long timeri = 0;


//setup arduino
void setup() {
  pinMode(sensorSignal, INPUT);
  pinMode(ledPin, OUTPUT);
  pinMode(valoPin, INPUT);
  Serial.begin(9600);
}


void loop() {
  //luetaan arvot
  sensorValue = digitalRead(sensorSignal);
  valoStatus = digitalRead(valoPin);
  ledStatus = digitalRead(ledPin);

  //Tutkitaan onko liikettä
  if (sensorValue == HIGH) {
    timer = millis();
    //tutkitaan onko pimeää, jos on, led syttyy
    if (valoStatus == HIGH) {
      digitalWrite(ledPin, HIGH);
      Serial.println("Valoisuus:" + String(valoStatus) + " Liike:" + String(sensorValue) + " Led:" + String(ledStatus));
    }
     else if (valoStatus == LOW) {
        digitalWrite(ledPin, LOW); 
        Serial.println("Valoisuus:" + String(valoStatus) + " Liike:" + String(sensorValue) + " Led:" + String(ledStatus));
     }
  }
  //Jos liikettä ei ole tiettyyn aikaan, led sammuu
  if (sensorValue == LOW) {
    timeri = millis();
    if (timeri - timer > 10000) {
      digitalWrite(ledPin, LOW);
      Serial.println("Valoisuus:" + String(valoStatus) + " Liike:" + String(sensorValue) + " Led:" + String(ledStatus));
    }
  }
  
}